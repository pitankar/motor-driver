# Motor Driver Library for Raspberry Pi

import RPi.GPIO as GPIO
from  time import sleep

# Class to hold all of the movement related methods
class MOVE:
	# pass the pins connected to the l293d driver. to control 2 motors there would be 4 output line.
	# method initializes the pins as output and makes the driver ready.
	def __init__( self, mt1_in1, mt1_in2, mt2_in1, mt2_in2 ):
		# set the pin naming convention
		GPIO.setmode(GPIO.BOARD)
		
		# copy the parameters
		self.mt1_in1 = mt1_in1; self.mt1_in2 = mt1_in2; self.mt2_in1 = mt2_in1; self.mt2_in2 = mt2_in2
		
		#set mode of all pins to output
		GPIO.setup( self.mt1_in1 , GPIO.OUT )
		GPIO.setup( self.mt1_in2 , GPIO.OUT )
		GPIO.setup( self.mt2_in1 , GPIO.OUT )
		GPIO.setup( self.mt2_in2 , GPIO.OUT )
		
		#stop all motors
		__write(0,0,0,0)	

		return 0


	# Method to turn around. 
	# Pass the direction. Pass the degrees by how much to rotate in case of left and right. Only pass the drection in case of forward and reverse.
	def turn( dir, deg = 0):
		# sanity check to ensure proper direction is provided
		if ( dir != "left" or dir != "right" or dir != "forward" or dir != "reverse" ):
			print "Incorrect value for DIRECTION provided. Returning."
			return -1
		
		# sanity check to ensure proper rotation value is provided.
		if ( deg < 0 or deg > 180 ):
			print "Incorrect value of angle for rotation provided. Returning."
			return -2
		
		# turn left
		if ( dir == "left" ):
			__write(0,0,0,0)
			sleep(deg)
			stop()

		# turn right
		if ( dir == "right" ):
			__write(0,0,0,0)
			sleep(deg)
			stop()	

		# move frward
		if ( dir == "forward" ):
			__write(0,0,0,0)

		# move backward
		if ( dir == "reverse" ):
			__write(0,0,0,0)

		return 0
	
	# Method to be called when both motor need to be stopped.
	def stop( self ):
		__write(0,0,0,0)
		return 0

	# Set the values of pins
	def __write( val_mt1_in1, val_mt1_in2, val_mt2_in1, val_mt2_in2 ):
		GPIO.output(self.mt1_in1, val_mt1_in1)
		GPIO.output(self.mt1_in2, val_mt1_in2)
		GPIO.output(self.mt2_in1, val_mt2_in1)
		GPIO.output(self.mt2_in2, val_mt2_in2)
		return 0

